<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluasi extends Model
{
    protected $table = 'kpi';
    protected $fillable = [
        'karyawan_id', 'laporan_evaluasi_id', 'value'
    ];

    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class, 'karyawan_id');
    }

    public function laporanEvaluasi()
    {
        return $this->belongsTo(Laporan::class, 'laporan_evaluasi_id');
    }
}
