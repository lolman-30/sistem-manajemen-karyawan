<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    use HasFactory;

    protected $table = 'biodata';
    protected $fillable = ['karyawan_id', 'alamat', 'nomor_telepon', 'tanggal_lahir', 'jenis_kelamin', 'foto_profil'];

    public function karyawan(){
        return $this->belongsTo(Karyawan::class);
    }

}
