<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    protected $table = 'laporan_evaluasi';
    protected $fillable = [
        'karyawan_id', 'tanggal', 'isi_laporan', 'jenis_laporan'
    ];

    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class, 'karyawan_id');
    }
}
