<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    protected $table = 'karyawan';
    protected $fillable = ['name', 'posisi', 'departemen_id'];

    public function departemen(){
        return $this->belongsTo(Departemen::class);
    }

    public function biodata(){
        return $this->hasOne(Biodata::class);
    }

}
