<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $users = User::all(['name', 'email']);
        $profile = Profile::all('alamat', 'no_telp');
        return view('profile.index', compact('users', 'profile'));
    }

    public function create()
    {
        $users = User::all();
        return view('profile.create', compact('users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'alamat' => 'required',
            'no_telp' => 'required',
        ]);

        Profile::create([
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp,
            'users_id' => $request->users_id,
        ]);

        return redirect('/profile')->with('success', 'Profil berhasil dibuat.');
    }

    public function show($id)
    {
        $user = User::find($id);
        dd($user);
        $profile = Profile::find($id);
        $name = $profile->user->name;
        return view('profile.index', compact('user', 'profile', 'name'));
    }
}
