<?php

namespace App\Http\Controllers;

use App\Models\Evaluasi;
use App\Models\Karyawan;
use App\Models\Laporan;
use Illuminate\Http\Request;

class EvaluasiController extends Controller
{
    public function index()
    {
        $evaluasi = Evaluasi::all();
        $title = 'Hapus Evaluasi!';
        $text = "Apakah anda yakin ingin menghapus entry ini?";
        confirmDelete($title, $text);
        return view('evaluasi.index', compact('evaluasi'));
    }

    public function create()
    {
        $karyawan = Karyawan::all();
        $laporan = Laporan::all();
        return view('evaluasi.create', compact('karyawan', 'laporan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'karyawan_id' => 'required',
            'laporan_evaluasi_id' => 'required',
            'value' => 'required',
        ]);

        Evaluasi::create($request->all());

        return redirect('/evaluasi')->with('success', 'Evaluasi berhasil ditambahkan.');
    }

    public function show($id)
    {
        $evaluasi = Evaluasi::findOrFail($id);
        return view('evaluasi.detail', compact('evaluasi'));
    }

    public function edit($id)
    {
        $evaluasi = Evaluasi::findOrFail($id);
        $karyawan = Karyawan::all();
        $laporan = Laporan::all();
        return view('evaluasi.edit', compact('evaluasi', 'karyawan', 'laporan'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'karyawan_id' => 'required',
            'laporan_evaluasi_id' => 'required',
            'value' => 'required',
        ]);

        $evaluasi = Evaluasi::findOrFail($id);
        $evaluasi->update($request->all());

        return redirect('/evaluasi')->with('success', 'Evaluasi berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $evaluasi = Evaluasi::findOrFail($id);
        $evaluasi->delete();

        return redirect('/evaluasi')->with('success', 'Evaluasi berhasil dihapus.');
    }
}