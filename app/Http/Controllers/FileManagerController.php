<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileManagerController extends Controller
{
    public function index()
    {

        $files = Storage::files('public/files');
        return view('file-manager.index', compact('files'));
    }

    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'required|file'
        ]);

        $request->file('file')->store('files', 'public');
        return back()->with('success', 'File uploaded successfully.');
    }

    public function delete($filename)
    {

        $filePath = 'public/files/' . $filename;

        Storage::delete($filePath);

        return back()->with('success', 'File deleted successfully.');
    }
}
