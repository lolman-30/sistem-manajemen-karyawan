<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Departemen;
use App\Models\Karyawan;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawan = Karyawan::all();
        $title = 'Hapus Karyawan!';
        $text = "Apakah anda yakin ingin menghapus entry ini?";
        confirmDelete($title, $text);
        return view('karyawan.index', ['karyawan' => $karyawan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departemen = Departemen::all();
        $biodata = Biodata::all();
        return view('karyawan.create', ['departemen' => $departemen, 'biodata' => $biodata]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'name' => 'required',
            'posisi' => 'required',
            'departemen_id' => 'required'
        ]);

        $karyawan = new Karyawan;
        $karyawan->name = $request['name'];
        $karyawan->posisi = $request['posisi'];
        $karyawan->departemen_id = $request['departemen_id'];
        $karyawan->save();
        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $karyawan = Karyawan::find($id);
        $biodata = Karyawan::find($karyawan->id)->biodata;
        //dd($karyawan);
        //dd($biodata);
        return view('karyawan.detail', ['karyawan'=>$karyawan, 'biodata' => $biodata]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $karyawanById = Karyawan::find($id);
        $departemen = Departemen::all();
        return view('karyawan.edit', ['karyawanById' => $karyawanById, 'departemen' => $departemen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'name' => 'required',
            'posisi' => 'required',
            'departemen_id' => 'required'
        ]);

        $karyawanById = Karyawan::find($id);
        $karyawanById->name = $request['name'];
        $karyawanById->posisi = $request['posisi'];
        $karyawanById->departemen_id = $request['departemen_id'];
        $karyawanById->save();
        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        if (isNull($karyawan->biodata)){
            $karyawan->delete();
        }    
        else {
            $karyawan->biodata->delete();
            $karyawan->delete();
        }
        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil dihapus');
    }
}
