<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Departemen;
use App\Models\Karyawan;
use Illuminate\Http\Request;

class DepartemenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departemen = Departemen::all();
        $title = 'Hapus Departemen!';
        $text = "Apakah anda yakin ingin menghapus entry ini?";
        confirmDelete($title, $text);
        return view('departemen.index', ['departemen'=> $departemen]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departemen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'nama_departemen' => 'required'
        ]);

        $departemen = new Departemen;
        $departemen->nama_departemen = $request->nama_departemen;
        $departemen->save();
        return redirect('/departemen')->with('success', 'Data Departemen Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departemen = Departemen::find($id);
        $biodata = Biodata::all();
        //dd($biodata);
        return view('departemen.detail', ['departemen' => $departemen, 'biodata' => $biodata]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departemen = Departemen::find($id);
        return view('departemen.edit', ['departemen' => $departemen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'nama_departemen' => 'required'
        ]);

        $departemen = Departemen::find($id);
        $departemen->nama_departemen = $request->nama_departemen;
        $departemen->save();
        return redirect('/departemen')->with('success', 'Data Departemen Berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departemen = Departemen::find($id);
        $departemen->delete();
        return redirect('/departemen')->with('success', 'Data Departemen Berhasil dihapus');
    }
}
