<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Laporan;
use App\Models\Karyawan;
use Barryvdh\DomPDF\Facade\Pdf;

class LaporanController extends Controller
{
    public function index()
    {
        $laporan = Laporan::all()->keyBy('id');
        $title = 'Hapus Laporan!';
        $text = "Apakah anda yakin ingin menghapus entry ini?";
        confirmDelete($title, $text);
        return view('laporan.index', compact('laporan'));
    }

    public function create()
    {
        $laporan = Laporan::all();
        $karyawan = Karyawan::all();
        return view('laporan.create', compact('laporan', 'karyawan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'karyawan_id' => 'required',
            'tanggal' => 'required',
            'isi_laporan' => 'required',
            'jenis_laporan' => 'required',
        ]);

        $laporan = Laporan::create([
            'karyawan_id' => $request->karyawan_id,
            'tanggal' => $request->tanggal,
            'isi_laporan' => $request->isi_laporan,
            'jenis_laporan' => $request->jenis_laporan,
        ]);

        $pdf = Pdf::loadView('pdf.invoice', compact('laporan'));
        $pdf->save(resource_path('filespdf/' . $laporan->id . '.pdf'));

        return redirect('/laporan')->with('success', 'Laporan berhasil ditambahkan.');
    }

    public function show($id)
    {
        $laporan = Laporan::find($id);
        return view('laporan.detail', compact('laporan'));
    }

    public function edit($id)
    {
        $laporan = Laporan::find($id);
        $karyawan = Karyawan::all();
        return view('laporan.edit', compact('laporan', 'karyawan'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'karyawan_id' => 'required',
            'tanggal' => 'required',
            'isi_laporan' => 'required',
            'jenis_laporan' => 'required',
        ]);

        $laporan = Laporan::find($id);
        $laporan->karyawan_id = $request->karyawan_id;
        $laporan->tanggal = $request->tanggal;
        $laporan->isi_laporan = $request->isi_laporan;
        $laporan->jenis_laporan = $request->jenis_laporan;
        $laporan->save();

        return redirect('/laporan')->with('success', 'Laporan berhasil diperbarui.');
    }


    public function destroy($id)
    {
        $laporan = Laporan::find($id);
        $laporan->delete();

        return redirect('/laporan')->with('success', 'Laporan berhasil dihapus.');
    }
}
