<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use App\Models\Departemen;
use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BiodataController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index()
    // {
    //     //        
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $karyawan = Karyawan::find($id);
        return view('biodata.create', ['karyawan' => $karyawan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request -> validate([
            'karyawan_id' => 'required',
            'alamat' => 'nullable',
            'nomor_telepon' => 'nullable',
            'tanggal_lahir' => 'nullable',
            'jenis_kelamin' => 'nullable', 
            'foto_profil' => 'required|mimes:jpg,jpeg,png|max:2048'
        ]);

        $fileName = time().'.'.$request->foto_profil->extension();
        $request->foto_profil->move(public_path('image'), $fileName);
        
        $biodata = new Biodata;
        $biodata->karyawan_id = $request->karyawan_id;
        $biodata->alamat = $request->alamat;
        $biodata->nomor_telepon = $request->nomor_telepon;
        $biodata->tanggal_lahir = $request->tanggal_lahir;
        $biodata->jenis_kelamin = $request->jenis_kelamin;
        $biodata->foto_profil = $fileName;
        //dd($biodata);
        $biodata->save();
        return redirect('/karyawan')->with('success', 'Biodata Berhasil ditambahkan');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $karyawan = Karyawan::find($id);
        $departemen = Departemen::all();
        return view('biodata.edit', ['karyawan' => $karyawan, 'departemen'=> $departemen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //UPDATE masih tidak mau post ke database
    public function update(Request $request, $id)
    {
        $request -> validate([
            'name' => 'required',
            'posisi' => 'required',
            'departemen_id' => 'required',
            'alamat' => 'nullable',
            'nomor_telepon' => 'nullable',
            'tanggal_lahir' => 'nullable',
            'jenis_kelamin' => 'nullable', 
            'foto_profil' => 'mimes:jpg,jpeg,png|max:2048'
        ]);

        $karyawan = Karyawan::find($id);
        if ($request->has('foto_profil')){
            File::delete(public_path("image/".$karyawan->biodata->foto_profil));
            
            $fileName = time().'.'.$request->foto_profil->extension();
            $request->foto_profil->move(public_path('image'), $fileName);

            $karyawan->biodata->foto_profil = $fileName;
        }
        $karyawan->name = $request->name;
        $karyawan->posisi = $request->posisi;
        $karyawan->departemen_id = $request->departemen_id;
        $karyawan->biodata->alamat = $request->alamat;
        $karyawan->biodata->nomor_telepon = $request->nomor_telepon;
        $karyawan->biodata->tanggal_lahir = $request->tanggal_lahir;
        $karyawan->biodata->jenis_kelamin = $request->jenis_kelamin;
        $karyawan->biodata->save();

        return redirect('/karyawan')->with('success', 'Biodata Berhasil Diperbarui');
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
