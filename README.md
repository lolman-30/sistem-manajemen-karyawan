# Final Project

# Kelompok 3

# Anggota Kelompok

1. Bagas Dwi Pranata
2. Rezy Ayatussyafi
3. Sofyan

# Tema Project

Sistem Manajemen Karyawan

## ERD

Gambar Entity-Relationship Diagram (ERD) dari Sistem Manajemen Karyawan

![ERD Sistem Manajemen Karyawan](/resources/images/erd-management-employee.png)

## Link

Link Demo Aplikasi : https://drive.google.com/file/d/1j_Bjv318nPTI4fBfh1ESR_J1CVBr1--x/view?usp=sharing
