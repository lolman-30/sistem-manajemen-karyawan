<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BiodataController;
use App\Http\Controllers\DepartemenController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\EvaluasiController;
use App\Http\Controllers\FileManagerController;

Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    //CRUD Departemen
    Route::resource('departemen', DepartemenController::class);
    //CRUD Karyawan
    Route::resource('karyawan', KaryawanController::class);
    //CRUD Profile
    Route::resource('profile', ProfileController::class);
    //CRUD Laporan
    Route::resource('laporan', LaporanController::class);
    //CRUD Evaluasi
    Route::resource('evaluasi', EvaluasiController::class);
    //CRUD Biodata
    Route::get('/karyawan/{id}/create-bio', [BiodataController::class, "create"]);
    Route::post('/karyawan/{id}', [BiodataController::class, "store"]);
    Route::get('/karyawan/{id}/edit-bio', [BiodataController::class, "edit"]);
    Route::put('/karyawan/{id}', [BiodataController::class, "update"]);
    // File Manager
    Route::get('/file-manager', [FileManagerController::class, 'index'])->name('file-manager');
    Route::post('/upload', [FileManagerController::class, 'upload'])->name('upload');
    Route::delete('/delete/{filename}', [FileManagerController::class, 'delete'])->name('delete');
});