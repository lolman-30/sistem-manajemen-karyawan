@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Edit Biodata Karyawan')

@section('content')
<form action="/karyawan/{{$karyawan->id}}" method="POST" enctype="multipart/form-data">
    @method('patch')
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$karyawan->name}}" readonly>
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Posisi</label>
      <input type="text" class="form-control @error('posisi') is-invalid @enderror" name="posisi" value="{{$karyawan->posisi}}" readonly>
      @error('posisi')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Departemen</label>
      <select name="departemen_id" class="form-control @error('departemen_id') is-invalid @enderror" readonly>
        @forelse ($departemen as $item)
            @if ($item->id === $karyawan->departemen_id)
            <option value="{{$item->id}}" selected>{{$item->nama_departemen}}</option> 
            @else
            {{-- <option value="{{$item->id}}">{{$item->nama_departemen}}</option>     --}}
            @endif
        @empty
            <option value="">Tidak Ada Data Departemen</option>
        @endforelse
      </select>
      @error('departemen_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" cols="30" rows="10">{{$karyawan->biodata->alamat}}</textarea>
        @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>No. Telepon</label>
        <input type="number" class="form-control @error('nomor_telepon') is-invalid @enderror" name="nomor_telepon" value="{{$karyawan->biodata->nomor_telepon}}">
        @error('nomor_telepon')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{$karyawan->biodata->tanggal_lahir}}">
        @error('tanggal_lahir')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Jenis Kelamin</label>
        <select name="jenis_kelamin" class="form-control @error('jenis_kelamin') is-invalid @enderror">
          <option value="">--Jenis Kelamin--</option>
          @if ($karyawan->biodata->jenis_kelamin == 'Pria')
            <option value="Pria" selected>Pria</option>
            <option value="Wanita">Wanita</option>
          @else
            <option value="Pria">Pria</option>
            <option value="Wanita" selected>Wanita</option>
          @endif
        </select>
        @error('jenis_kelamin')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Foto Profil</label><br>
        <img src="{{asset('image/'. $karyawan->biodata->foto_profil)}}" style="height: 100px" class="my-2">
        <input type="file" class="form-control @error('foto_profil') is-invalid @enderror" name="foto_profil">
        @error('foto_profil')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
  <button type="submit" class="btn btn-primary my-2">Submit</button>
</form>
@endsection