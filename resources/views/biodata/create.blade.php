@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Lengkapi Biodata')

@section('content')
<form action="/karyawan/{{$karyawan->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Karyawan</label><br>
        <select name="karyawan_id" class="form-control">
            <option value="{{$karyawan->id}}" selected>{{$karyawan->name}}</option>
        </select>
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat">
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>No. Telepon</label>
        <input type="text" class="form-control @error('nomor_telepon') is-invalid @enderror" name="nomor_telepon">
        @error('nomor_telepon')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir">
        @error('tanggal_lahir')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Jenis Kelamin</label><br>
        <select name="jenis_kelamin" class="form_control @error('jenis_kelamin') is-invalid @enderror">
            <option value="Pria">Pria</option>
            <option value="Wanita">Wanita</option>
        </select>
        @error('jenis_kelamin')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group my-2">
        <label>Upload Foto</label>
        <input type="file" class="form-control @error('foto_profil') is-invalid @enderror" name="foto_profil">
        @error('foto_profil')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary my-2">Submit</button>
</form>
@endsection