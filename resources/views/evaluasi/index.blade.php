@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Evaluasi')
@section('title', 'Evaluasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header">
                    <a href="{{ url('/evaluasi/create') }}" class="btn btn-primary">Tambah Evaluasi</a>
                    <a href="{{ route('file-manager') }}" class="btn btn-secondary">File Manager</a>
                </div>
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="d-none d-xl-table-cell">Nama Karyawan</th>
                            <th class="d-none d-md-table-cell">Laporan</th>
                            <th class="d-none d-xl-table-cell">Nilai</th>
                            <th class="d-none d-md-table-cell">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($evaluasi as $index => $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td class="d-none d-md-table-cell">{{ $item->karyawan->name }}</td>
                                <td class="d-none d-xl-table-cell">
                                    <a href="{{ route('laporan.show', $item->laporanEvaluasi->id) }}">Lihat Laporan</a>
                                </td>
                                <td class="d-none d-xl-table-cell">{{ $item->value }}</td>
                                <td>
                                    <a href="{{ route('evaluasi.show', $item->id) }}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="{{ route('evaluasi.edit', $item->id) }}"
                                        class="btn btn-warning btn-sm">Edit</a>
                                    <form action="{{ route('evaluasi.destroy', $item->id) }}" method="POST"
                                        style="display:inline;">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('evaluasi.destroy', $item->id) }}" class="btn btn-danger btn-sm" data-confirm-delete="true">Hapus</a>
                                        {{-- <button type="button" class="btn btn-danger btn-sm swal-confirm"
                                            data-confirm-title="Delete Item"
                                            data-confirm-text="Are you sure you want to delete this item?">Delete</button> --}}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
