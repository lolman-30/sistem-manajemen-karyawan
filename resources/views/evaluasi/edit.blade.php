@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Evaluasi')
@section('title', 'Edit Evaluasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{ route('evaluasi.update', $evaluasi->id) }}">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="nama">Karyawan:</label>
                        <select name="karyawan_id" id="karyawan_id"
                            class="form-control @error('karyawan_id') is-invalid @enderror">
                            <option value="">--Pilih Karyawan--</option>
                            @foreach ($karyawan as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == $item->karyawan_id ? 'selected' : '' }}>
                                    {{ $item->name }}
                                </option>
                            @endforeach
                        </select>
                    </div><br>

                    <div class="form-group">
                        <label for="KPI">Laporan Evaluasi: </label>
                        <select name="laporan_evaluasi_id" class="form-control"
                            @error('laporan_evaluasi_id') is-invalid @enderror>
                            <option value="">--Pilih Laporan--</option>
                            @foreach ($laporan as $item)
                                <option value="{{ $item->id }}"
                                    {{ $item->id == $evaluasi->laporan_evaluasi_id ? 'selected' : '' }}>
                                    {{ $item->isi_laporan }}
                                </option>
                            @endforeach
                        </select>
                    </div><br>

                    <div class="form-group">
                        <label for="value">Nilai Evaluasi</label>
                        <input type="text" class="form-control" id="value" name="value"
                            value="{{ $evaluasi->value }}">
                    </div><br>

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
