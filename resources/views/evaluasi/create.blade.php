@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Evaluasi')
@section('title', 'Tambah Evaluasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{ route('evaluasi.store') }}">
                    @csrf

                    <div class="form-group">
                        <label for="nama">Karyawan:</label>
                        <select name="karyawan_id" id="karyawan_id"
                            class="form-control @error('karyawan_id') is-invalid @enderror">
                            <option value="">--Pilih Karyawan--</option>
                            @foreach ($karyawan as $person)
                                <option value="{{ $person->id }}">{{ $person->name }}</option>
                            @endforeach
                        </select>
                    </div><br>

                    <div class="form-group">
                        <label for="KPI">Laporan Evaluasi: </label>
                        <select name="laporan_evaluasi_id" class="form-control"
                            @error('laporan_evaluasi_id') is-invalid @enderror>
                            <option value="">--Pilih Laporan--</option>
                            @forelse ($laporan as $item)
                                <option value="{{ $item->id }}">{{ $item->isi_laporan }}</option>
                            @empty
                                <option value="">Tidak Ada Laporan</option>
                            @endforelse
                        </select>
                    </div><br>

                    <div class="form-group">
                        <label for="value">Nilai Evaluasi</label>
                        <input type="text" class="form-control" id="value" name="value">
                    </div><br>

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // Fetch and populate laporan evaluasi based on selected karyawan
        document.getElementById('karyawan_id').addEventListener('change', function() {
            var karyawanId = this.value;
            var laporanSelect = document.getElementById('laporan_evaluasi_id');
            laporanSelect.innerHTML = ''; // Clear existing options

            if (karyawanId !== '') {
                // Make an AJAX request to fetch laporan evaluasi for the selected karyawan
                fetch('/api/laporan-evaluasi/' + karyawanId)
                    .then(response => response.json())
                    .then(data => {
                        data.forEach(laporan => {
                            var option = document.createElement('option');
                            option.value = laporan.id;
                            option.textContent = laporan.judul;
                            laporanSelect.appendChild(option);
                        });
                    });
            }
        });
    </script>
@endsection
