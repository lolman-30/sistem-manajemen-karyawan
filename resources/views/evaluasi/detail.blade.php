@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Detail Evaluasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Detail Evaluasi</h2>
                <p><strong>Nama Karyawan:</strong> {{ $evaluasi->karyawan->name }}</p>
                <p><strong>Isi Laporan:</strong> {{ $evaluasi->laporanEvaluasi->isi_laporan }}</p>
                <p><strong>Nilai:</strong> {{ $evaluasi->value }}</p>
                <!-- You can add more details here if needed -->
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-8">
                <a href="{{ url('/evaluasi') }}" class="btn btn-secondary">Kembali</a>
            </div>
        </div>
    </div>
@endsection
