<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Invoice</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 14px;
            margin: 0;
            padding: 0;
        }

        .container {
            padding: 20px;
        }

        .title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .label {
            font-weight: bold;
        }

        .details {
            margin-top: 20px;
        }

        /* Tambahkan gaya CSS tambahan di sini sesuai kebutuhan Anda */
    </style>
</head>

<body>
    <div class="container">
        <div class="title text-center mb-4">Laporan Invoice</div>
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="label">ID Laporan:</td>
                            <td>{{ $laporan->id }}</td>
                        </tr>
                        <tr>
                            <td class="label">Karyawan:</td>
                            <td>{{ $laporan->karyawan->name }}</td>
                        </tr>
                        <tr>
                            <td class="label">Tanggal:</td>
                            <td>{{ $laporan->tanggal }}</td>
                        </tr>
                        <tr>
                            <td class="label">Jenis Laporan:</td>
                            <td>{{ $laporan->jenis_laporan }}</td>
                        </tr>
                        <tr>
                            <td class="label">Isi Laporan:</td>
                            <td>{{ $laporan->isi_laporan }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
