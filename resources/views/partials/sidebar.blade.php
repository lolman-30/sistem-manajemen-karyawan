<div class="wrapper">
    <nav id="sidebar" class="sidebar js-sidebar">
        <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="{{ url('/') }}">
                <span class="align-middle">Sistem Manajemen</span>
            </a>
            @auth
                <ul class="sidebar-nav">
                    <li class="sidebar-header">
                        Pages
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="/">
                            <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="/departemen">
                            <i class="align-middle" data-feather="users"></i> <span class="align-middle">Departemen</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="/karyawan">
                            <i class="align-middle" data-feather="user-check"></i> <span
                                class="align-middle">Karyawan</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="{{ url('laporan') }}">
                            <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Laporan</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="{{ url('evaluasi') }}">
                            <i class="align-middle" data-feather="clipboard"></i> <span class="align-middle">Evaluasi</span>
                        </a>
                    </li>
                </ul>
            @endauth


        </div>
    </nav>
