@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Edit Data Karyawan')

@section('content')
<form action="/karyawan/{{$karyawanById->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$karyawanById->name}}">
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Posisi</label>
      <input type="text" class="form-control @error('posisi') is-invalid @enderror" name="posisi" value="{{$karyawanById->posisi}}">
      @error('posisi')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Departemen</label>
      <select name="departemen_id" class="form-control @error('departemen_id') is-invalid @enderror">
        <option value="">--Pilih Departemen--</option>
        @forelse ($departemen as $item)
            @if ($item->id === $karyawanById->departemen_id)
            <option value="{{$item->id}}" selected>{{$item->nama_departemen}}</option> 
            @else
            <option value="{{$item->id}}">{{$item->nama_departemen}}</option>    
            @endif
        @empty
            <option value="">Tidak Ada Data Departemen</option>
        @endforelse
      </select>
      @error('departemen_id')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary my-2">Submit</button>
</form>
@endsection