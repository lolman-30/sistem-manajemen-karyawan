@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Detail Karyawan')

@section('content')
    <h1 class="text-primary">{{$karyawan->name}}</h1>
    <h2 class="text-secondary">{{$karyawan->posisi}}</h2>
    {{-- Buat nanti disambung biodata --}}
    @if (is_null($biodata))
        <a href="/karyawan/{{$karyawan->id}}/create-bio" class="btn btn-secondary btn-sm">Tambah Biodata</a>
    
        <h3 class="text-center">--BELUM ADA BIODATA--</h3>
    @else
    <a href="/karyawan/{{$karyawan->id}}/edit-bio" class="btn btn-warning btn-sm">Edit Biodata</a>

    <form class="my-2">
        <div class="form-group">
            <label>Alamat</label>
            <input type="text" class="form-control" placeholder="{{$biodata->alamat}}" readonly>
        </div>
        <div class="form-group">
            <label>No. Telepon</label>
            <input type="text" class="form-control" placeholder="{{$biodata->nomor_telepon}}" readonly>
        </div>
        <div class="form-group">
            <label>Tanggal Lahir</label>
            <input type="text" class="form-control" placeholder="{{$biodata->tanggal_lahir}}" readonly>
        </div>
        <div class="form-group">
            <label>Jenis Kelamin</label>
            <input type="text" class="form-control" placeholder="{{$biodata->jenis_kelamin}}" readonly>
        </div>
    @endif
@endsection