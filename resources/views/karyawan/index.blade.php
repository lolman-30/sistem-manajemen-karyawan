@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Data Karyawan')

@section('content')
<a href="/karyawan/create" class="btn btn-primary btn-sm">Tambah Data Karyawan</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Karyawan</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($karyawan as $keys => $item)
        <tr>
            <th scope="row">{{$keys + 1}}</th>
            <td>{{$item->name}}</td>
            <td>
                <form action="/karyawan/{{$item->id}}" method="post">
                  @csrf
                  @method('delete')
                  <a href="/karyawan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/karyawan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <a href="/karyawan/{{$item->id}}" class="btn btn-danger btn-sm" data-confirm-delete="true">Delete</a>
                </form>
            </td>
        </tr>
        @empty
          <tr colspan="3">
            <td>Tidak Ada Data Karyawan</td>
          </tr>   
        @endforelse
    </tbody>
  </table>
@endsection