@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Tambah Data Karyawan')

@section('content')
    <form action="/karyawan" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div><br>
        <div class="form-group">
            <label>Posisi</label>
            <input type="text" class="form-control @error('posisi') is-invalid @enderror" name="posisi">
            @error('posisi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div><br>
        <div class="form-group">
            <label>Departemen</label>
            <select name="departemen_id" class="form-control @error('departemen_id') is-invalid @enderror">
                <option value="">--Pilih Departemen--</option>
                @forelse ($departemen as $item)
                    <option value="{{ $item->id }}">{{ $item->nama_departemen }}</option>
                @empty
                    <option value="">Tidak Ada Data Departemen</option>
                @endforelse
            </select>
            @error('departemen_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary my-2">Submit</button>
    </form>
@endsection
