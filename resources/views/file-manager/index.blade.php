@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || File Manager')
@section('title', 'File Manager')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <form action="{{ route('upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">Upload File</label>
                        <input type="file" class="form-control-file" id="file" name="file">
                    </div><br>
                    <button class="btn btn-info" type="submit">Upload</button>
                </form>
            </div>
        </div>
        <br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">File Name</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($files as $file)
                    <tr>
                        <td>{{ basename($file) }}</td>
                        <td>
                            <a href="{{ Storage::url($file) }}" class="btn btn-primary btn-sm">View</a>
                            <form action="{{ route('delete', ['filename' => basename($file)]) }}" method="post"
                                style="display:inline;">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger btn-sm swal-confirm" type="submit"
                                    onclick="return confirm('Are you sure you want to delete this file?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
