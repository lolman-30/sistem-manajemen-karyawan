@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Dashboard')

@section('content')
    @auth
        <h1>Selamat Datang di Demo Sistem Manajemen Karyawan!</h1>
    @endauth

    @guest
        <h1 class="text-center">Untuk Menggunakan Demo Sistem Manajemen Karyawan</h1>
        <h1 class="text-center">Anda Harus Login Terlebih Dahulu</h1>
    @endguest
@endsection