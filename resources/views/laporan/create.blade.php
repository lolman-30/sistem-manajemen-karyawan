@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Laporan')
@section('title', 'Tambah Laporan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form action="{{ url('/laporan') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama:</label>
                        <select name="karyawan_id" class="form-control @error('karyawan_id') is-invalid @enderror">
                            <option value="">--Pilih Karyawan--</option>
                            @forelse ($karyawan as $person)
                                <option value="{{ $person->id }}">{{ $person->name }}</option>
                            @empty
                                <option value="">Tidak Ada Karyawan</option>
                            @endforelse
                        </select>
                    </div><br>
                    <div class="form-group">
                        <label for="tanggal">Tanggal:</label>
                        <input type="date" name="tanggal" class="form-control">
                    </div><br>
                    <div class="form-group">
                        <label for="isi_laporan">Isi Laporan:</label>
                        <textarea name="isi_laporan" class="form-control" rows="5" placeholder="Silakan tuliskan isi laporan di sini..."></textarea>
                        <small id="isi_laporan_help" class="form-text text-muted">Tulis laporan dengan jelas dan rinci agar
                            dapat dipahami oleh pihak terkait.</small>
                    </div><br>
                    <div class="form-group">
                        <label for="jenis_laporan">Jenis Laporan:</label>
                        <input type="text" name="jenis_laporan" class="form-control">
                    </div><br>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
