@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Laporan')
@section('title', 'Laporan')

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 col-xxl-9 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <a href="{{ url('/laporan/create') }}" class="btn btn-primary">Tambah Laporan</a>
                </div>
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th class="d-none d-xl-table-cell">Tanggal</th>
                            <th class="d-none d-md-table-cell">Laporan</th>
                            <th class="d-none d-md-table-cell">Jenis Laporan</th>
                            <th class="d-none d-md-table-cell">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan as $index => $lap)
                            <tr>
                                <td>{{ $index }}</td>
                                <td class="d-none d-md-table-cell">{{ $lap->karyawan->name }}</td>
                                <td class="d-none d-xl-table-cell">{{ $lap->tanggal }}</td>
                                <td>{{ $lap->isi_laporan }}</td>
                                <td>{{ $lap->jenis_laporan }}</td>
                                <td>
                                    <a href="{{ url('/laporan/' . $lap->id) }}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="{{ url('/laporan/' . $lap->id . '/edit') }}"
                                        class="btn btn-warning btn-sm">Edit</a>
                                    <form action="{{ url('/laporan/' . $lap->id) }}" method="POST" style="display:inline;">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ url('/laporan/' . $lap->id) }}" class="btn btn-danger btn-sm" data-confirm-delete="true">Hapus</a>
                                        {{-- <button type="submit" class="btn btn-danger btn-sm"
                                            onclick="return confirm('Yakin ingin menghapus?')">Hapus</button> --}}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
