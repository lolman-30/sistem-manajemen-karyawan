@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Detail Laporan')

@section('content')
    <div class="mt-3">
        <div>
            <strong>Nama:</strong> {{ $laporan->karyawan->nama }} <br>
            <strong>Tanggal:</strong> {{ $laporan->tanggal }} <br>
            <strong>Isi Laporan:</strong> {{ $laporan->isi_laporan }} <br>
            <strong>Jenis Laporan:</strong> {{ $laporan->jenis_laporan }} <br>
        </div>
    </div><br>
    <a href="{{ url('/laporan') }}" class="btn btn-secondary">Kembali</a>
@endsection
