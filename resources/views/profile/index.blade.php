@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Profile')
@section('title', 'Profile')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        @if ($users->isNotEmpty())
                            @foreach ($users as $user)
                                <h3 class="card-title mb-4">{{ $user->name }}</h3>
                                <p><strong>Email:</strong> {{ $user->email }}</p>

                                @foreach ($profile as $data)
                                    <p><strong>Alamat:</strong> {{ $data->alamat }}</p>
                                    <p><strong>No. Telepon:</strong> {{ $data->no_telp }}</p>
                                @endforeach
                                <a href="{{ route('profile.create') }}" class="btn btn-info">Isi Data Diri</a>
                            @endforeach
                        @else
                            <p>Tidak ada data pengguna.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
