@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Profile')
@section('title', 'Edit Profile')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        @if ($user)
                            <form action="{{ route('profile.update', $user->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name:</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ $user->name }}" disabled>
                                </div>

                                <div class="mb-3">
                                    <label for="email" class="form-label">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ $user->email }}" disabled>
                                </div>

                                <div class="mb-3">
                                    <label for="alamat" class="form-label">Alamat:</label>
                                    <input type="text" class="form-control" id="alamat" name="alamat"
                                        value="{{ $profile->alamat }}" required>
                                </div>

                                <div class="mb-3">
                                    <label for="nomor_telepon" class="form-label">Nomor Telepon:</label>
                                    <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon"
                                        value="{{ $profile->no_telp }}" required>
                                </div>

                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        @else
                            <p>Tidak ada data pengguna.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
