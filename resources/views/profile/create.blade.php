@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen || Profile')
@section('title', 'Isi Data Diri')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form action="{{ route('profile.store') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="alamat" class="form-label">Alamat: </label>
                        <input type="text" class="form-control" id="alamat" name="alamat" required>
                    </div>
                    <div class="mb-3">
                        <label for="no_telp" class="form-label">Nomor Telepon: </label>
                        <input type="text" class="form-control" id="no_telp" name="no_telp" required>
                    </div>
                    <div class="mb-3">
                        <label for="users_id" class="form-label">Nama: </label>
                        <select class="form-select" id="users_id" name="users_id" required>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
