@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Data Departemen')

@section('content')
<a href="/departemen/create" class="btn btn-primary btn-sm">Tambah Departemen</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Departemen</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($departemen as $keys => $item)
        <tr>
            <th scope="row">{{$keys + 1}}</th>
            <td>{{$item->nama_departemen}}</td>
            <td>
                <form action="/departemen/{{$item->id}}" method="post">
                  @csrf
                  @method('delete')
                  <a href="/departemen/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/departemen/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <a href="/departemen/{{$item->id}}" class="btn btn-danger btn-sm" data-confirm-delete="true">Delete</a>
                </form>
            </td>
        </tr>
        @empty
          <tr colspan="3">
            <td>Tidak Ada Data Departemen</td>
          </tr>   
        @endforelse
    </tbody>
  </table>
@endsection