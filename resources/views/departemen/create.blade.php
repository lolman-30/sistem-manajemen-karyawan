@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Tambah Departemen')

@section('content')
<form action="/departemen" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Departemen</label>
      <input type="text" class="form-control @error('nama_departemen') is-invalid @enderror" name="nama_departemen">
      @error('nama_departemen')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary my-2">Submit</button>
</form>
@endsection