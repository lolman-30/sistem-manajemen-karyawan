@extends('layouts.master')

@section('tab-title', 'Demo Sistem Manajemen')
@section('title', 'Detail Departemen')

@section('content')
    <h1 class="text-primary"> {{$departemen->nama_departemen}} </h1>
    <h2>List Karyawan</h2>
    
    <div class="container">
        <div class="row">
            @forelse ( $departemen->karyawan as $item)
                <div class="col-4">
                    <div class="card">
                        <img src="{{asset('/image/'.$item->biodata->foto_profil)}}" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h2>{{$item->name}}</h2>
                            <h3>{{$item->posisi}}</h3>
                        </div>
                      </div>
                </div>
            @empty
            <h4 class="text-center">--Tidak Ada Karyawan di Departemen Ini--</h4>    
            @endforelse
        </div>
    </div>
@endsection